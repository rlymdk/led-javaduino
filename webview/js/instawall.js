$(document).ready(function()
{
	$('#mainbody').css({'width' : WIDTH, 'height' : HEIGHT});
	
	var sceneQueue = 
	[
		[1, 500, 20000],
		[2, 0, 36000]
	];

	function gameScenario()
	{
		var sceneTimeChunk = 250,
			time 			= 0,
			sceneInterval,
			sceneIndex 		= 0,
			isSceneIn		= false,
			animatedEvent 	= 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		
		sceneInterval = setInterval(function()
		{
			if (sceneIndex >= sceneQueue.length)
			{
				clearInterval(sceneInterval);
				return;
			}

			if (!isSceneIn)
			{
				if (sceneQueue[sceneIndex][1] == 0)
				{
					animateScene(sceneQueue[sceneIndex][0], 'IN');
					isSceneIn = true;
				}
				sceneQueue[sceneIndex][1] -= sceneTimeChunk;
			}
			if (isSceneIn)
			{
				sceneQueue[sceneIndex][2] -= sceneTimeChunk;
				if (sceneQueue[sceneIndex][2] == 0)
				{
					animateScene(sceneQueue[sceneIndex][0], 'OUT');
					sceneIndex++;
				}
			}
			time += sceneTimeChunk;
		}, sceneTimeChunk);
	
		function animateScene(scene, action)
		{
			switch (scene)
			{
				case 1:
					var text1PC = 'text-1',
						text1 	= $('#scene-1 .' + text1PC),
						text2PC = 'text-2',
						text2 	= $('#scene-1 .' + text2PC),
						text3PC = 'text-3',
						text3 	= $('#scene-1 .' + text3PC),
						text4PC = 'text-4',
						text4 	= $('#scene-1 .' + text4PC),
						text5PC = 'text-5',
						text5 	= $('#scene-1 .' + text5PC),
						text6PC = 'text-6',
						text6 	= $('#scene-1 .' + text6PC),
						text7PC = 'text-7',
						text7 	= $('#scene-1 .' + text7PC),
						text8PC = 'text-8',
						text8 	= $('#scene-1 .' + text8PC);
	
					if (action === 'IN')
					{
						$('#scene-1').show();
						text1.css({'visibility' : 'visible'}).addClass('animated bounceInDown');

						setTimeout(function()
						{
							text2.css({'visibility' : 'visible'}).addClass('animated bounceInDown');
						}, 200);
						setTimeout(function()
						{
							text3.css({'visibility' : 'visible'}).addClass('animated bounceInDown');
						}, 200 * 2);
						setTimeout(function()
						{
							text4.css({'visibility' : 'visible'}).addClass('animated bounceInDown');
						}, 200 * 3);
						setTimeout(function()
						{
							text5.css({'visibility' : 'visible'}).addClass('animated bounceInDown');
						}, 200 * 4);
						setTimeout(function()
						{
							text7.css({'visibility' : 'visible'}).addClass('animated bounceInUp');
						}, 200 * 4);
						setTimeout(function()
						{
							text8.css({'visibility' : 'visible'}).addClass('animated bounceInUp');
						}, 200 * 5);
						setTimeout(function()
						{
							text6.css({'visibility' : 'visible'}).addClass('animated bounceInLeft');
						}, 1500);
					}
					else if (action === 'OUT')
					{
						text6.addClass('animated bounceOutRight').one(animatedEvent, function()
						{
							$(this).attr('class', text6PC).css({'visibility' : 'hidden'});
						});
						setTimeout(function()
						{
							text1.addClass('animated bounceOutUp').one(animatedEvent, function()
							{
								$(this).attr('class', text1PC).css({'visibility' : 'hidden'});
							});
							text2.addClass('animated bounceOutUp').one(animatedEvent, function()
							{
								$(this).attr('class', text2PC).css({'visibility' : 'hidden'});
							});
							text3.addClass('animated bounceOutUp').one(animatedEvent, function()
							{
								$(this).attr('class', text3PC).css({'visibility' : 'hidden'});
							});
							text4.addClass('animated bounceOutUp').one(animatedEvent, function()
							{
								$(this).attr('class', text4PC).css({'visibility' : 'hidden'});
							});
							text5.addClass('animated bounceOutUp').one(animatedEvent, function()
							{
								$(this).attr('class', text5PC).css({'visibility' : 'hidden'});
							});
							text7.addClass('animated bounceOutDown').one(animatedEvent, function()
							{
								$(this).attr('class', text7PC).css({'visibility' : 'hidden'});
							});
							text8.addClass('animated bounceOutDown').one(animatedEvent, function()
							{
								$(this).attr('class', text7PC).css({'visibility' : 'hidden'});
								$('#scene-1').hide();
								isSceneIn = false;
							});
						}, 500);
					}
				break;
				case 2:
					if (action === 'IN')
					{
						$('#scene-2').show();
						$('#scene-2 .hashtag').fadeIn(2000);
						show();
						setInterval(function(){
							hide();
						}, 11000);
					}
					else if (action === 'OUT')
					{
						$('#scene-2 .hashtag').fadeOut(2000, function()
						{
							$('#scene-2').hide();
						});
					}
				break;
			}
		}
	}

	gameScenario();

});

function show()
{
	$("#grid img").each(function()
	{
		$(this).animate({'opacity':0}, {}, 0);
	}).promise().done(function()
	{
		storm();
	});
}

function hide()
{
	var d = 0; 		// delay
	var ry, tz, s; 	// transform params 
	
	// fading out the thumbnails with style
	$("#grid img").each(function()
	{
		d = Math.random() * 1000;	//1ms to 1000ms delay
		$(this).delay(d).animate({opacity: 0},
		{
			// while the thumbnails are fading out, we will use the step function to apply some transforms.
			// variable n will give the current opacity in the animation.
			step: function(n)
			{
				s = 1 - n;	//scale - will animate from 0 to 1
				$(this).css("transform", "scale("+ s +")");
			},
			complete: function()
			{
				var current = $(this).data('step');
				if (++current > 3)
					return;
				var photo 	= $(this).data('img' + (current));
				$(this).attr('src', photo).data('step', current);
				$(this).animate({opacity: 1},
				{
					step: function(n)
					{
						s = n;
						$(this).css("transform", "scale("+ s +")");
					}, 
					duration: 1000,
				})
			},
			duration: 1000, 
		})
	});	
}

// bringing back the images with style
function storm()
{
	$("#grid img").each(function()
	{
		d = Math.random() * 1000;
		$(this).delay(d).animate({opacity: 1},
		{
			step: function(n)
			{
				// rotating the images on the Y axis from 360deg to 0deg
				ry = (1 - n) * 360;
				// translating the images from 1000px to 0px
				tz = (1 - n) * 1000;
				// applying the transformation
				$(this).css("transform", "rotateY("+ ry +"deg) translateZ("+ tz +"px)");
			}, 
			duration: 3000, 
			// some easing fun. Comes from the jquery easing plugin.
			easing: 'easeOutQuint', 
		})
	})
}
