$(document).ready(function()
{
	$('#mainbody').css({'width' : WIDTH, 'height' : HEIGHT});

	function gameScenario()
	{
		var sceneTimeChunk = 250,
			time 			= 0,
			sceneInterval,
			sceneIndex 		= 0,
			isSceneIn		= false,
			animatedEvent 	= 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		
		sceneInterval = setInterval(function()
		{
			if (sceneIndex >= sceneQueue.length)
			{
				clearInterval(sceneInterval);
				return;
			}

			if (!isSceneIn)
			{
				if (sceneQueue[sceneIndex][1] == 0)
				{
					animateScene(sceneQueue[sceneIndex][0], 'IN');
					isSceneIn = true;
				}
				sceneQueue[sceneIndex][1] -= sceneTimeChunk;
			}
			if (isSceneIn)
			{
				sceneQueue[sceneIndex][2] -= sceneTimeChunk;
				if (sceneQueue[sceneIndex][2] == 0)
				{
					animateScene(sceneQueue[sceneIndex][0], 'OUT');
					sceneIndex++;
				}
			}
			time+=sceneTimeChunk;
		}, sceneTimeChunk);
	
		function animateScene(scene, action)
		{
			switch (scene)
			{
				case 1:
					var textPC 	= 'text-1',
						text 	= $('#scene-1 .' + textPC);
	
					if (action === 'IN')
					{
						$('#scene-1').show();
						text.addClass('animated zoomIn');
					}
					else if (action === 'OUT')
					{
						text.attr('class', textPC).addClass('animated flipOutY');
						text.one(animatedEvent, function()
						{
							$('#scene-1').hide();
							text.attr('class', textPC);
							isSceneIn = false;
						});
					}
				break;
				case 2:
					var text1PC = 'text-1',
						text1 	= $('#scene-2 .' + text1PC),
						text2PC = 'text-2',
						text2 	= $('#scene-2 .' + text2PC),
						text3PC = 'text-3',
						text3 	= $('#scene-2 .' + text3PC);
	
					if (action === 'IN')
					{
						$('#scene-2').show();
						text2.addClass('animated zoomIn');
						setTimeout(function()
						{
							text1.css({'visibility' : 'visible'}).addClass('animated bounceInDown');
							text3.css({'visibility' : 'visible'}).addClass('animated bounceInUp');
						}, 500);
					}
					else if (action === 'OUT')
					{
						text1.attr('class', text1PC).addClass('animated bounceOutUp');
						text3.attr('class', text3PC).addClass('animated bounceOutDown');
	
						text1.one(animatedEvent, function()
						{
							$(this).attr('class', text1PC).css({'visibility' : 'hidden'});
						});
						text3.one(animatedEvent, function()
						{
							$(this).attr('class', text3PC).css({'visibility' : 'hidden'});
						});
	
						text2.attr('class', text2PC);
						setTimeout(function()
						{
							text2.addClass('animated rotateOut').one(animatedEvent, function()
							{
								$(this).attr('class', text2PC);
								$('#scene-2').hide();
								isSceneIn = false;
							});
						}, 750);
					}
				break;
				case 3:
					var text1PC = 'text-1',
						text1 	= $('#scene-3 .' + text1PC),
						phonePC = 'phone',
						phone 	= $('#scene-3 .' + phonePC),
						handPC 	= 'hand',
						hand 	= $('#scene-3 .' + handPC);
	
					if (action === 'IN')
					{
						$('#scene-3').show();
						text1.addClass('animated bounceInLeft');
						phone.addClass('animated bounceInRight');
						setTimeout(function()
						{
							hand.show().addClass('animated bounceInDown');
							setTimeout(function()
							{
								hand.attr('class', handPC).addClass('animated bounce');
							}, 3000);
						}, 1000);
					}
					else if (action === 'OUT')
					{
						hand.addClass('bounceOutDown').one(animatedEvent, function()
						{
							$(this).hide().attr('class', handPC);
						});
						setTimeout(function()
						{
							phone.addClass('bounceOutRight').one(animatedEvent, function()
							{
								$(this).attr('class', phonePC);
							});
							text1.addClass('bounceOutLeft').one(animatedEvent, function()
							{
								$(this).attr('class', text1PC);
								$('#scene-3').hide();
								isSceneIn = false;
							});
						}, 500);
					}
				break;
				case 5:
					var text1PC = 'text-1',
						text1 	= $('#scene-5 .' + text1PC),
						text2PC = 'text-2',
						text2 	= $('#scene-5 .' + text2PC),
						text3PC = 'text-3',
						text3 	= $('#scene-5 .' + text3PC),
						text4PC = 'text-4',
						text4 	= $('#scene-5 .' + text4PC);
	
					if (action === 'IN')
					{
						$('#scene-5').show();
						text1.css({'visibility' : 'visible'}).addClass('animated bounceInDown');
						setTimeout(function()
						{
							text2.css({'visibility' : 'visible'}).addClass('animated bounceInLeft');
							text4.css({'visibility' : 'visible'}).addClass('animated bounceInRight');
						}, 1000);
						setTimeout(function()
						{
							text3.css({'visibility' : 'visible'}).addClass('animated zoomIn');
						}, 2500);
					}
					else if (action === 'OUT')
					{
						text1.attr('class', text1PC).addClass('animated bounceOutUp');
						text2.attr('class', text2PC).addClass('animated bounceOutRight');
						text4.attr('class', text4PC).addClass('animated bounceOutLeft');
	
						text1.one(animatedEvent, function()
						{
							$(this).attr('class', text1PC).css({'visibility' : 'hidden'});
						});
						text2.one(animatedEvent, function()
						{
							$(this).attr('class', text2PC).css({'visibility' : 'hidden'});
						});
						text4.one(animatedEvent, function()
						{
							$(this).attr('class', text4PC).css({'visibility' : 'hidden'});
						});
	
						text3.attr('class', text3PC);
						setTimeout(function()
						{
							text3.addClass('animated rotateOut').one(animatedEvent, function()
							{
								$(this).attr('class', text3PC).css({'visibility' : 'hidden'});
								$('#scene-5').hide();
								isSceneIn = false;
							});
						}, 750);
					}
				break;
				case 6:
					var text1PC = 'text-1',
						text1 	= $('#scene-6 .' + text1PC)
						text2PC = 'text-2',
						text2 	= $('#scene-6 .' + text2PC)
						photos 	= $('#scene-6 #player-photos');

					if (action === 'IN')
					{
						$('#scene-6').show();
						text1.addClass('animated bounceInLeft');
						text2.addClass('animated bounceInRight');
						setTimeout(function()
						{
							photos.css({'visibility' : 'visible'}).addClass('animated zoomIn');
						}, 500);
					}
					else if (action === 'OUT')
					{
						text1.attr('class', text1PC).addClass('animated bounceOutRight');
						text2.attr('class', text2PC).addClass('animated bounceOutLeft');

						setTimeout(function()
						{
							photos.addClass('animated zoomOut').one(animatedEvent, function()
							{
								$(this).css({'visibility' : 'hidden'}).attr('class', '');
								text1.attr('class', text1PC);
								text2.attr('class', text2PC);
								$('#scene-6').hide();
								console.log(time);
							});
						}, 500);
					}
				break;
			}
		}
	}

	gameScenario();

});
