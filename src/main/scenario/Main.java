package main.scenario;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import main.MainFrame;

public class Main {

	static JFrame f;

	public static void main(String[] a) {
		f = new JFrame();
		f.setLayout(null);

		JScrollPane scrollPane = new JScrollPane(new DragDropList());
		scrollPane.setBounds(25, 20, 250, 350);

		JButton nextBtn = new JButton("Запустить");
		nextBtn.setBounds(25, 380, 250, 50);
		nextBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				f.setVisible(false);
				new MainFrame();
			}
		});

		f.add(scrollPane);
		f.add(nextBtn);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);
		f.setSize(305, 500);
		f.setVisible(true);
	}

	public static ArrayList<String> getInitValues() {
		ArrayList<String> arr = new ArrayList<String>();

		try {
			Scanner in = new Scanner(new File("scenario.txt"));
			if (in.hasNext()) {
				int n = in.nextInt();
				in.nextLine();
				for (int i = 0; i < n; i++) {
					arr.add(in.nextLine());
				}
			}
			
			//Adding videos to scenario
			File folder = new File("video/");
			File[] listOfFiles = folder.listFiles();
			String[] filesName = new String[listOfFiles.length];
			for (int i = 0; i < filesName.length; i++) {
				filesName[i] = "VIDEO_" + listOfFiles[i].getName();
			}
			Arrays.sort(filesName, new AlphanumericSorting());
			for (int i = 0; i < filesName.length; i++) {
				if (!arr.contains(filesName[i])) {
					arr.add(filesName[i]);
				}
			}
			
			//Adding apps to scenario
//			File folderApps = new File("apps/");
//			String[] listOfApps = folderApps.list(new FilenameFilter() {
//				  @Override
//				  public boolean accept(File current, String name) {
//					  return new File(current, name).isDirectory();
//				  }
//				});
//			String[] appsName = new String[listOfApps.length];
//			for (int i = 0; i < appsName.length; i++) {
//				appsName[i] = "APP_" + listOfApps[i];
//			}
//			for (int i = 0; i < appsName.length; i++) {
//				if (!arr.contains(appsName[i])) {
//					arr.add(appsName[i]);
//				}
//			}
			in.close();


			PrintWriter out = new PrintWriter(new File("scenario.txt"));
			String output = arr.size() + System.getProperty("line.separator");
			for (int i = 0; i < arr.size(); i++) {
				output += arr.get(i) + System.getProperty("line.separator");
			}
			out.write(output);
			out.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return arr;
	}

	public static void generateScenario(String[] arr) {

		try {
			PrintWriter out = new PrintWriter(new File("scenario.txt"));
			String output = arr.length + "\n";
			for (int i = 0; i < arr.length; i++) {
				output += arr[i] + "\n";
			}
			out.write(output);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
