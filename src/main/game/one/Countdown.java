package main.game.one;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import main.MainFrame;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Enumeration;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;


@SuppressWarnings("serial")
public class Countdown extends JPanel implements SerialPortEventListener{

	SerialPort serialPort;
	/**
	 * A BufferedReader which will be fed by a InputStreamReader converting the
	 * bytes into characters making the displayed results codepage independent
	 */
	private BufferedReader input;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;
	/** Default port for usage */
	private static final String PORT_NAME = "COM3";
	static CommPortIdentifier portId = null;

	@SuppressWarnings("rawtypes")
	static Enumeration portEnum;
	
	String count[] =  {"  1 ",  "  2 ", "  3 "};
	JLabel label;
	private int i = 0;
	private ImageIcon circle = new ImageIcon("images/circle.png");
	private Timer t;
	JLabel textLabel;

	public Countdown() {
		initializeSerialConnection();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		ge.getAllFonts();
		
		Font font = new Font("Jokerman", Font.PLAIN, 50);
		Font font1 = new Font("Jokerman", Font.PLAIN, 90);
		
		textLabel = new JLabel("PRESS ANY KEY TO START");
		textLabel.setFont(font);
		textLabel.setBounds(15, 150, 2000, 200);
		add(textLabel);

		t = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				textLabel.setFont(font1);
				textLabel.setBounds(290, 290, 150, 150);
				if (i < 3){
					textLabel.setText(count[i]);
					i++;
				} else{
					t.stop();
					closeSerialConnection();
					MainFrame.setPanel("GAMEPANEL", "");
				}
			}
		});
		
		setLayout(null);
		setFocusable(true);
		addKeyListener(new MyKeyHandler());
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		circle.paintIcon(null, g, 280, 290);
	}
	
	private class MyKeyHandler extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				t.start();
			}
		}
	}
	
	public void initializeSerialConnection() {

		portEnum = CommPortIdentifier.getPortIdentifiers();

		// First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			if (currPortId.getName().equals(PORT_NAME)) {
				portId = currPortId;
				break;
			}
		}
		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			// output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port. This will prevent
	 * port locking on platforms like Linux.
	 */
	public synchronized void closeSerialConnection() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine = input.readLine();
				if (inputLine.equals("one-left") || inputLine.equals("one-right") || inputLine.equals("two-left") || inputLine.equals("two-right")) {
					t.start();
				}
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
		// Ignore all the other eventTypes, but you should consider the other
		// ones.
	}
}
