package main.game.one;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Enumeration;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import main.MainFrame;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements SerialPortEventListener {

	/**
	 * Serial connection variables
	 */
	SerialPort serialPort;
	/**
	 * A BufferedReader which will be fed by a InputStreamReader converting the
	 * bytes into characters making the displayed results codepage independent
	 */
	private BufferedReader input;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;
	/** Default port for usage */
	private static final String PORT_NAME = "COM3";
	/** Moving distance per click */
	private static final int MOV_DIS = 10;

	static CommPortIdentifier portId = null;

	@SuppressWarnings("rawtypes")
	static Enumeration portEnum;

	/**
	 * Game logic variables
	 */

	// Variable for timer
	private Timer t;
	// Get background image
	ImageIcon bg = new ImageIcon("images/bg.png");
	// Get sprite sheet
	private BufferedImage[] player1b = { SpriteSheet.getSprite(0, 0), SpriteSheet.getSprite(7, 0) };
	private BufferedImage[] player2b = { SpriteSheet.getSprite(0, 0), SpriteSheet.getSprite(7, 0) };
	// Create animation for sprite sheets
	private Animation player1ani = new Animation(player1b, 7);
	private Animation player2ani = new Animation(player2b, 7);
	// Variables for game logic
	private int X1 = 0, X2 = 0;
	private boolean qPressed = false, wPressed = false, uPressed = false, iPressed = false;

	public GamePanel() {
		setLayout(null);
		setFocusable(true);
		setBackground(Color.GREEN);
		addKeyListener(new MyKeyHandler());
		initUI();
		initializeSerialConnection();
		setRequestFocusEnabled(true);
		grabFocus();
	}

	private void initUI() {
		t = new Timer(10, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player1ani.update();
				player2ani.update();
				repaint();
				if (X1 >= 600) {
					endGame("Congratulations! Player 1 is a winner!");
				}
				if (X2 >= 600) {
					endGame("Congratulations! Player 2 is a winner!");
				}
			}
		});

		t.start();
	}

	public void endGame(String message) {
		t.stop();
		closeSerialConnection();
		player1ani.stop();
		player2ani.stop();
		X1 = 0;
		X2 = 0;
		qPressed = false;
		wPressed = false;
		uPressed = false;
		iPressed = false;
		MainFrame.setPanel("WINNERPANEL", message);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		bg.paintIcon(null, g, 0, 145);
		g.drawImage(player1ani.getSprite(), X1, 300, null);
		g.drawImage(player2ani.getSprite(), X2, 390, null);
	}

	private class MyKeyHandler extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_Q && !qPressed) {
				X1 += MOV_DIS;
				player1ani.start();
				qPressed = true;
				wPressed = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_W && !wPressed) {
				X1 += MOV_DIS;
				player1ani.start();
				qPressed = false;
				wPressed = true;
			}
			if (e.getKeyCode() == KeyEvent.VK_U && !uPressed) {
				X2 += MOV_DIS;
				player2ani.start();
				uPressed = true;
				iPressed = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_I && !iPressed) {
				X2 += MOV_DIS;
				player2ani.start();
				uPressed = false;
				iPressed = true;
			}
			repaint();
		}

		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_Q) {
				player1ani.stop();
			}
			if (e.getKeyCode() == KeyEvent.VK_W) {
				player1ani.stop();
			}
			if (e.getKeyCode() == KeyEvent.VK_U) {
				player2ani.stop();
			}
			if (e.getKeyCode() == KeyEvent.VK_I) {
				player2ani.stop();
			}
		}
	}

	public void initializeSerialConnection() {

		portEnum = CommPortIdentifier.getPortIdentifiers();

		// First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			if (currPortId.getName().equals(PORT_NAME)) {
				portId = currPortId;
				break;
			}
		}
		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			// output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port. This will prevent
	 * port locking on platforms like Linux.
	 */
	public synchronized void closeSerialConnection() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine = input.readLine();
				System.out.println(inputLine + " 1 :" + X1);
				System.out.println(inputLine + " 2 :" + X2);

				if (inputLine.equals("one-left") || inputLine.equals("one-right")) {
					player1ani.start();
					X1 = X1 + MOV_DIS;
				}
				if (inputLine.equals("one-up"))
					player1ani.stop();
				if (inputLine.equals("two-left") || inputLine.equals("two-right")) {
					player2ani.start();
					X2 = X2 + MOV_DIS;
				}
				if (inputLine.equals("two-up"))
					player2ani.stop();
				repaint();
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
		// Ignore all the other eventTypes, but you should consider the other
		// ones.
	}
}
