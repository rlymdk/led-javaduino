package main.game.one;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class EndGamePanel extends JPanel{
	
	private JLabel winnerLabel;
	private Font font;
	
	public EndGamePanel(String message){
		init();
		winnerLabel.setText(message);
		add(winnerLabel);
	}
	
	public void init(){
		setLayout(null);
		setFocusable(true);
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		ge.getAllFonts();
		font = new Font("Jokerman", Font.PLAIN, 35);
		winnerLabel = new JLabel();
		winnerLabel.setBounds(30, 150, 2000, 150);
		winnerLabel.setFont(font);
		winnerLabel.setForeground(Color.ORANGE);
	}
}
