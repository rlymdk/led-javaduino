package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import com.coremedia.iso.IsoFile;
import com.xuggle.xuggler.IContainer;

public class Config {
	public static int WIDTH = 720, HEIGHT = 770;
	public static int gameTime = 20;
	public static String panelQueue[];
	public static int panelDuration[];
	public static int panelCurrent = 0;

	public static void init() {
		try {
			Scanner in = new Scanner(new File("scenario.txt"));
			int n = in.nextInt();
			in.nextLine();

			panelQueue = new String[n];
			panelDuration = new int[n];

			for (int i = 0; i < n; i++) {
				panelQueue[i] = in.nextLine();
				System.out.println(panelQueue[i]);
				if (panelQueue[i].substring(0, 6).equalsIgnoreCase("VIDEO_")) {
					panelDuration[i] = (int) Math
							.ceil(getVideoLength((new File("video/"
									+ panelQueue[i].substring(6))).getPath()));
				} else if(panelQueue[i].substring(0, 4).equalsIgnoreCase("APP_")) {
					panelDuration[i] = gameTime;
				}
			}
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static double getVideoLength(String path) {
		double resultDouble = 0;

		if (path.substring(path.length() - 4).equalsIgnoreCase(".mp4")
				|| path.substring(path.length() - 4).equalsIgnoreCase(".3gp")
				|| path.substring(path.length() - 4).equalsIgnoreCase(".mov")) {
			try {
				IsoFile isoFile = new IsoFile(path);
				resultDouble = (double) isoFile.getMovieBox()
						.getMovieHeaderBox().getDuration()
						/ isoFile.getMovieBox().getMovieHeaderBox()
								.getTimescale();
				isoFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			// first we create a Xuggler container object
			IContainer container = IContainer.make();

			// we attempt to open up the container
			int result = container.open(path, IContainer.Type.READ, null);

			// check if the operation was successful

			if (result >= 0) {
				resultDouble = ((double) container.getDuration()) / 1000000;
				container.close();
			}
			container = null;
		}
		return resultDouble;
	}
}
