package main.rotator;

import java.io.File;

import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;

public class Rotator {

	public EmbeddedMediaPlayerComponent mediaPlayerComponent;

	public Rotator() {
		
		mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
	}

	public void play(String file) {
		mediaPlayerComponent.getMediaPlayer().playMedia((new File("video/"+file)).getPath());
	}

	public void stop() {
		mediaPlayerComponent.getMediaPlayer().stop();
		//mediaPlayerComponent = null;
	}
}
