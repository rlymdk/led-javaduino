package main;

import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.JFrame;

import main.game.one.Countdown;
import main.game.one.EndGamePanel;
import main.game.one.GamePanel;
import main.rotator.Rotator;
import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	
	static int loopTime = 0;
	static Rotator rotator;
	static GamePanel gamePanel;
	static Countdown countPanel; 
	static EndGamePanel winnerPanel;
	public static Container getContentPane;

	public MainFrame() {
		// VLC
		NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(),
				"C:\\Program Files\\VideoLAN\\VLC");
		
		Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);

		Audio.init();
		Config.init();
		
		
		getContentPane = getContentPane();
		rotator = new Rotator();
		countPanel = new Countdown();

		setSize(Config.WIDTH, Config.HEIGHT);
		setVisible(true);
//		setSize(2 * getWidth() - getContentPane().getWidth(), 2 * getHeight()
//				- getContentPane().getHeight());

		//set size and position (delete resizing then after knowing resolution)
		
		//setSize and position from file
		try {
			Scanner in = new Scanner(new File("position.txt"));
			if(in.hasNext()){
				int x = in.nextInt();
				int y = in.nextInt();
				int width = in.nextInt();
				int height = in.nextInt();
				setLocation(x, y);
				setSize(width, height);
			}
			in.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		//end of set sizing and position from file
		
		//save parameters when close
		addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        try {
					PrintWriter out = new PrintWriter(new File("position.txt"));
					out.write(getLocation().x+" "+getLocation().y+" "+getSize().width+" "+getSize().height);
					out.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
		        System.exit(0);
		    }
		});
		//end of save parameters when close
		
		// change location by arrows
		addKeyListener(new KeyListener() {

			int x = getLocation().x;
			int y = getLocation().y;
			int shift = 5;

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_UP: {
					y -= shift;
					break;
				}
				case KeyEvent.VK_DOWN: {
					y += shift;
					break;
				}
				case KeyEvent.VK_RIGHT: {
					x += shift;
					break;
				}
				case KeyEvent.VK_LEFT: {
					x -= shift;
					break;
				}
				}
				setLocation(x, y);
			}
		});
		// end of change location by arrows

		//set size and position (delete resizing then after knowing resolution)
		
		for (int i = 0; i < Config.panelDuration.length; i++) {
			loopTime += Config.panelDuration[i];
		}

		RotatorLoop.init(loopTime);
		/**
		 * Main thread which switches between application panels
		 */
		new Thread(new Runnable() {

			@Override
			public void run() {
				RotatorLoop.startTimer();
				while (true) {
					if (!RotatorLoop.panelIsActive) {
						RotatorLoop.panelIsActive = true;
						Audio.updateVolume();

						// Video rotator
						if (Config.panelQueue[Config.panelCurrent].substring(0,
								6).equalsIgnoreCase("VIDEO_")) {
							rotator.stop();

							// Change panel
							getContentPane().removeAll();
							getContentPane().add(rotator.mediaPlayerComponent);
							getContentPane().revalidate();

							rotator.play(Config.panelQueue[Config.panelCurrent]
									.substring(6));

							Audio.stopAll();
						}
						
						//Game rotator
						else if (Config.panelQueue[Config.panelCurrent]
								.substring(0, 4).equalsIgnoreCase("APP_")){
							
							rotator.stop();
							
							setPanel("COUNTPANEL", "");
							
							Audio.stopAll();
						}

						System.gc();
					}
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	public static void setPanel(String game, String message){
		if(game.equals("GAMEPANEL")){
			gamePanel = new GamePanel();
			getContentPane.removeAll();;
			getContentPane.add(gamePanel);
			gamePanel.requestFocus();
			getContentPane.revalidate();
		}
		if(game.equals("COUNTPANEL")){
			countPanel = new Countdown();
			getContentPane.removeAll();
			getContentPane.add(countPanel);
			countPanel.requestFocus();
			getContentPane.revalidate();
		}
		if(game.equals("WINNERPANEL")){
			winnerPanel = new EndGamePanel(message);
			getContentPane.removeAll();
			getContentPane.add(winnerPanel);
			winnerPanel.requestFocus();
			getContentPane.revalidate();
		}
		
	}
}
